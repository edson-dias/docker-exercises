#!/bin/bash

docker volume create db_dados
docker container run -d --name nginx1  -v db_dados:/usr/share/nginx/html nginx:latest
docker container run -d --name nginx2  -v db_dados:/usr/share/nginx/html nginx:latest
docker build --tag ex_01
docker container run -ti -v db_dados:/usr/share/nginx/html ex_01
cd usr/share/nginx/html/
nano index.html